## Internal Documentation for developing the project codename RektifiedFire

This is not same as any other Documentation as it contains the internal files necessary for developing the game "Depth Walker".

#### What does it contain?
It contains files such as...

+ Game's story
+ "Screenplay" script
+ Logos and other core branding
+ Planning for development

## What is even "Depth Walker"?
Depth Walker, directed by Kabir Akzaman and developed by the OpenNullanoid community contributors, is an exhilarating Sci-Fi Action-Adventure First Person Shooter (FPS) project.

In this thrilling game, players assume the role of RektifierKX, an advanced robot agent developed by Mechmorphix, an esteemed AI technology company working in partnership with the Law Enforcement agencies of Kioxia city.

Within Kioxia, the notorious criminal organization known as DarkWave has been engaging in illegal activities, supplying cutting-edge technology to criminals. When DarkWave's human personnel evaded capture, they left behind sensitive information guarded by their dangerous machinery.

As RektifierKX, players must embark on a stealth-based infiltration mission into the abandoned DarkWave facility. Using their exceptional skills, players must gather crucial intel while avoiding detection, unraveling the criminal organization's secrets.

Through intense action, strategic decision-making, and immersive stealth gameplay, players will explore the depths of the facility, uncovering a web of corruption and criminal activities that must be stopped.

Will RektifierKX outsmart DarkWave, retrieve vital information, and restore order to Kioxia? Prepare for a captivating journey filled with danger, intrigue, and high-tech espionage.

## What about the licences?
